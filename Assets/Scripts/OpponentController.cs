﻿using UnityEngine;
using UnityEngine.AI;

public class OpponentController : MonoBehaviour
{

    public NavMeshAgent agent;
    public Transform levelEnd;

    void Start()
    {
        agent.SetDestination(levelEnd.position);
    }

}
