﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Player playerInput;
    private Animator animator;


    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    [SerializeField]
    private float playerSpeed = 3.0f;
    private float gravityValue = -9.81f;

    private void Awake()
    {

        playerInput = new Player();
        controller = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
    }

    //Yeni input sisteminin çalışması için gerekli
    private void OnEnable()
    {
        playerInput.Enable();
    }
    private void OnDisable()
    {
        playerInput.Disable();
    }


    void Update()
    {   
        //Yere basıyor mu kontrolü
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            //Eğer yere basıyor ve dikey hızı 0dan büyükse dikey hızı 0la
            playerVelocity.y = 0f;   
        }
        
        //Yeni imput sistemiyle Joystick veya WASD inputunun alınması
       Vector2 movementInput = playerInput.PlayerMain.Move.ReadValue<Vector2>();
        
       Vector3 move = new Vector3(movementInput.x,0f,movementInput.y);
        //Animasyonlar Joystick ileri-geri inputuna bağlı 
        animator.SetFloat("Speed", Mathf.Abs(movementInput.y));
        
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        //Hız güncellemeleri
        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }

    /*Character controller kullandığım için Rigidbody kullanamıyorum
    bu unityinin fizik motorundan yararlanmamanın yanında sadece 
    karakterin hareket ettiği yöne doğru collision checke imkan veriyor*/
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.tag == "Obstacle")
        {
              //Engele çarptın yeniden doğ
              FindObjectOfType<GameManager>().Respawner(transform);
        }

        if (hit.collider.tag == "LevelEnd")
        {
            //Oyun bitti
            FindObjectOfType<GameManager>().ShowEndUI();
        }

    }

}
