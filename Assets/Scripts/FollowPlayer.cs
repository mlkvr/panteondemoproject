﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;

    // Basit bir kamera takibi verilen değerleri ekleyerek kendini oyuncunun pozisyonuna sabitliyor
    void Update()
    {
        transform.position = player.position + offset;
    }
}
