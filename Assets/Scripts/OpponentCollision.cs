﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentCollision : MonoBehaviour
{

    private void OnCollisionEnter(Collision hit)
    {
        if (hit.collider.tag == "Obstacle")
        {
            FindObjectOfType<GameManager>().Respawner(transform);
        }
    }
}
