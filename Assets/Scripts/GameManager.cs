﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    GameObject[] Opponents;
    GameObject[] racersAll;
    public GameObject player;

    public Transform respawnPoint;

    public Text rank;
    public Text endRank;
    public GameObject panel;

    private void Start()
    {
        //Sıralama icin tüm yapay zeka karakterlerin eklenmesi
        Opponents = GameObject.FindGameObjectsWithTag("Opponent");
        //Oyuncununda eklenebilmesi için +1 yere sahip ikinci dizi
        racersAll = new GameObject[Opponents.Length + 1];
        for (int i = 0; i < Opponents.Length; i++)
        {
            //ikinci diziye yapay zeka karakterler ekleniyor
            racersAll[i] = Opponents[i];
        }
        //son indexe oyuncu ekleniyor
        racersAll[Opponents.Length] = player;
    }


    private void Update()
    {
        //Sondan başa tüm oyuncuların Z değerine göre sıralanması
        for (int i = racersAll.Length - 1; i > 0; i--)
        {
            if (racersAll[i].transform.position.z > racersAll[i-1].transform.position.z)
            {
                GameObject temp = racersAll[i - 1];
                racersAll[i - 1] = racersAll[i];
                racersAll[i] = temp;
            }
        }

        //Sıralanmış listede oyuncunun aranması
        for (int i = 0; i < racersAll.Length; i++)
        {
            if (racersAll[i] == player)
            {
                rank.text = (i + 1) + ".";
            }
        }
          

    }

    public void Respawner(Transform obj)
    {
        //Engellere çarptığında veya platformdan düştüğünde yeniden doğmasını sağlar.
        obj.transform.position = respawnPoint.transform.position;
        Physics.SyncTransforms();

    }

    public void ShowEndUI()
    {
        //Oyun sonu bilgilendirme paneli
        panel.SetActive(true);
        endRank.text = rank.text;
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
