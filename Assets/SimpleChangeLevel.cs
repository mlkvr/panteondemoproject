﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimpleChangeLevel : MonoBehaviour
{

    public void ChangeLevel()
    {
        //Menü için basit bir bölüm değiştirme fonksiyonu
        SceneManager.LoadScene("SampleScene");
    }
}
